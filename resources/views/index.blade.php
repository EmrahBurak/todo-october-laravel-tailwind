@extends('layouts.defaul')


@section('content')
<section class="mb-6">
    <div class="max-w-screen-md mx-auto">
        <form action="{{ url('/todo')}}" method="POST">
            @csrf
            <div class="flex justify-center rounded shadow @error('title') border border-red-500
            @enderror">
                <input type="text" name="title" id="title" class="w-full px-4 py-2 rounded-l outline-none">
                <input type="submit" value="Create" class="px-4 py-2 bg-purple-500 rounded-l">
            </div>
            @error('title')
                <p class="mt-3 text-red-500"> Please enter a value..</p>

            @enderror
        </form>
    </div>
</section>
<section>
    <div class="max-w-screen-md mx-auto">
        @foreach ($todos as $todo )
        <div class="flex justify-between p-4 mb-3 bg-white shadow rounded-xl">
            <div class="title">
                {{$todo->title}}
            </div>
            <form action="{{route('todo.destroy',$todo)}}" method="POST">
                @csrf
                @method("DELETE")
                <button type="submit" class="text-red-500">Delete</button>
            </form>
        </div>

        @endforeach

    </div>

</section>



@endsection
